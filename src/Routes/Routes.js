import React, { Suspense } from 'react'
import { withRouter, Route } from 'react-router-dom';

import {Recipes,DetailedRecipe,  AddRecipes  } from '../Pages'


const Routes = () => {
    return (
        <Suspense>
            <Route exact path = "/" component={Recipes}/>
            <Route exact path = '/recipe' component = {DetailedRecipe}/>
            <Route exact path = '/addrecipes' component = {AddRecipes}/>

         

        </Suspense>
    )
}

export default withRouter(Routes)
