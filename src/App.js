import { BrowserRouter } from 'react-router-dom';
import Routes from './Routes/Routes'
import Header from './Components/Header/Header'

import 'antd/dist/antd.css'; 
import './App.css';


function App() {
  return (
    <div className="App">
      <Header/>
      <BrowserRouter>
        <Routes />
      </BrowserRouter>
    </div>
  );
}

export default App;