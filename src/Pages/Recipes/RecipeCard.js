import React from 'react'
import { Link } from 'react-router-dom'

import { Card, Modal } from 'antd';
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';
import foodImage from '../../assets/images/italian_meatballs--s.jpg'
import './RecipeCard.css'

const { Meta } = Card;


const RecipeCard = (props) => {

  return (
    <Card
      className="receipes-card"
      style={{ width: 250 }}
      cover={
        <img
          alt="Food-images"
          src={foodImage}
        />
      }

    >
      <Meta className='recipes-title-section'
        title={props.foodList.title}
        description={props.foodList.description}
      />
      <div>
        {/* Serving & Cook time */}
        <section className='serving-section'>
          <span>Serving size :</span>
          <span> {props.foodList.servings}</span>
        </section>
        <section>
          <span>Preparation Time :</span>
          <span> {props.foodList.prepTime} mins</span>
        </section>
        <section>
          <span>Cooking Time :</span>
          <span> {props.foodList.cookTime}</span>

        </section>
      </div>

    </Card>
  )
}

export default RecipeCard
