import React, { Component } from 'react';
import RecipeCard from './RecipeCard';

import { Row, Col, Button } from 'antd'

import axios from 'axios';
import { Link } from 'react-router-dom';

import { Constants } from '../../Constants.js'
import './Recipes.css'

const URL = Constants.localUrl

class Recipes extends Component {
    constructor(props) {
        super(props)

        this.state = {
            foodList: [],
            isLoaded: false,
            error: null
        }
    }

    componentDidMount() {
        axios.get(`${URL}` + "recipes").then((response) => {
            this.setState({
                isLoaded: true,
                foodList: response.data
            });
        },
            error => {
                this.setState({
                    isLoaded: true,
                    error: "Network Error",
                });
            })
    }


    render() {
        const { foodList, isLoaded, error } = this.state;

        return (

            <div className="recipes">
                {!isLoaded ? <div className = "status-message">Loading..</div> : (error ? <div className = "status-message">{error}</div> :
                    <Row gutter={[24, 48]} justify="center">
                        {foodList && foodList.map((list, ind) => {
                            return <Col
                                onClick={() => {
                                    this.props.history.push({
                                        pathname: '/recipe',
                                        state: list.uuid
                                    })
                                }}
                                key={list.uuid} xs={17} sm={12} md={10} xl={7}>
                                <RecipeCard foodList={list} />
                            </Col>
                        })}

                    </Row>)}
                <div className="addrecipe-button">
                    <Button type="primary" onClick={() => { this.props.history.push("/addrecipes") }}>Add Recipes</Button>
                </div>
            </div>
        )

    }
}

export default Recipes