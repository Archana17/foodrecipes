import React, { Component } from 'react'
import { Form, Input, Row, Col, Button,Modal } from 'antd'

import axios from 'axios'
import './AddRecipes.css'

class AddRecipes extends Component {

    addformRef = React.createRef()

    constructor(props) {
        super(props)

        this.state = {
            error: null,

        }
    }

    onFinish = (values) => {
        axios.post("http://localhost:3001/recipes", values).then((response) => {
            console.log(response.data);
        }, error => {
            this.setState({
                error: "Network Error"
            })
        })
    }

    render() {
        return (
            <div className="add-recipes">
                {this.state.error ?
                    <div>
                        <h3>{this.state.error}</h3>
                    </div> :
                    (<>
                        <h2>Add Recipe</h2>
                        <Row justify="center">
                            <Col xs={24} sm={20} md={10}>
                                <Form
                                    onFinish={this.onFinish.bind(this)}
                                    ref={this.addformRef}
                                    layout='vertical'
                                    spellCheck="false">
                                    <Form.Item
                                        name="title"
                                        label="Title"
                                        rules={[{
                                            required: true,
                                            message: 'Please enter title',
                                        },
                                        ]}
                                    >

                                        <Input />
                                    </Form.Item>
                                    <Form.Item
                                        name="description"
                                        label="Description"
                                        rules={[

                                            {
                                                required: true,
                                                message: 'Please enter Description',
                                            }
                                        ]}
                                    >

                                        <Input />
                                    </Form.Item>
                                    <Form.Item
                                        name="servings"
                                        label="Servings"
                                        rules={[
                                            {
                                                pattern: new RegExp(/^[0-9]+$/),
                                                message: 'Please enter valid value',
                                            },

                                            {
                                                required: true,
                                                message: 'Please enter Servings quantity',
                                            }
                                        ]}
                                    >

                                        <Input />
                                    </Form.Item>
                                    <Form.Item
                                        name="prepTime"
                                        label="Preparation Time"
                                        rules={[
                                            {
                                                pattern: new RegExp(/^[0-9]+$/),
                                                message: 'Please enter valid value!',
                                            },

                                            {
                                                required: true,
                                                message: 'Please enter Preparation Time!',
                                            }
                                        ]}
                                    >

                                        <Input />
                                    </Form.Item>
                                    <Form.Item
                                        name="cookTime"
                                        label="Cook Time"
                                        rules={[
                                            {
                                                pattern: new RegExp(/^[0-9]+$/),
                                                message: 'Please enter valid value!',
                                            },

                                            {
                                                required: true,
                                                message: 'Please enter cook Time!',
                                            }
                                        ]}
                                    >
                                        <Input />
                                    </Form.Item>
                                    <Button htmlType="submit" type = "primary"> Add </Button>


                                </Form>
                            </Col>
                        </Row> </>
                    )}


            </div>
        )
    }
}
export default AddRecipes