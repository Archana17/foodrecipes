import React, { Component } from 'react'
import { Image,Row,Col } from 'antd'
import recipeImage from '../../assets/images/italian_meatballs.jpg'
import axios from 'axios'
import { Constants } from '../../Constants.js'

import './DetailedRecipe.css'

const URL = Constants.localUrl


class DetailedRecipe extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.location.state,
            recipe: {},
            specialRecipe: [],
            newingredients: [],
            isLoaded: false,
            error: null


        }
    }


    componentDidMount() {
        axios.get(`${URL}` + "recipes").then((response) => {
            axios.get(`${URL}` + "specials").then((response1) => {
                const recipeindex = response.data.findIndex(recipe => (recipe.uuid === this.state.id));
                console.log(recipeindex, 33);
                this.setState({
                    recipe: response.data[recipeindex],
                    specialRecipe: response1.data,
                    isLoaded: true

                }, () => { this.binddata() });
            })
        }, error => {
            this.setState({
                isLoaded: true,
                error: error.message,
            });
        })
    }

    binddata = () => {
        this.state.recipe.ingredients.map((ingredient, ind) => {
            const updated = this.state.specialRecipe.find((special) =>
                special.ingredientId === ingredient.uuid
            )
            if (updated) {
                ingredient.type = updated.type;
                ingredient.title = updated.title;
                ingredient.text = updated.text;

            }

            this.state.newingredients.push(ingredient);
            this.setState(prevState => ({
                newingredients: [...prevState.newingredients], ingredient
            }))

        })
    }


    render() {
        const { id, recipe, specialRecipe, newingredients, isLoaded, error } = this.state

        return (
            <div className="detailed-recipe">
                {!isLoaded ? <div>Loading..</div> : (error ? <div>Error:{error}</div> :
                    <>

                        <Image src={recipeImage} preview={false} />
                        <div className="main-section">
                            <section
                                className="basic-info">
                                <h3>{recipe.title}</h3>
                                <p>{recipe.description}</p>
                                <div>
                                    <label>Servings : {recipe.servings} Nos </label>
                                    <label> | Prep Time : {recipe.prepTime} mins</label>
                                    <label> | Cook Time : {recipe.cookTime} mins</label>

                                </div>
                            </section>

                            <section className = "ingredients">
                                <h4> Ingredients</h4>
                                <Row gutter={[24, 48]} justify="center">

                                {newingredients && newingredients.map((ingredientlist, ind) => {
                                    return <Col xs={17} sm={12} md={10} xl={7}>
                                        {ingredientlist.name ? <div> Name : {ingredientlist.name} </div> : null}
                                        {ingredientlist.title ? <div> Title :{ingredientlist.title} </div> : null}
                                        {ingredientlist.amount ? <div> Amount : {ingredientlist.amount} </div> : null}
                                        {ingredientlist.measurement ? <div> Measurement : {ingredientlist.measurement} </div> : null}
                                        {ingredientlist.type ? <div> Type :  {ingredientlist.type} </div> : null}

                                    </Col>
                                })}
                                </Row>
                            </section>

                            <section className="direction">
                                <h4> Directions</h4>
                                <ol>
                                    {recipe.directions && recipe.directions.map((step, ind) => {
                                        return (
                                            <div>
                                                <li> {step.instructions}
                                                </li>
                                            </div>
                                        )
                                    })}
                                </ol>
                            </section>

                        </div>
                    </>)}
            </div>
        )
    }
}

export default DetailedRecipe