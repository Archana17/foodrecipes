# README #

Developed Food recipes ReactJs application for the Given API (json-server) server. To install and run, use the commands below:

npm i

npm start


1.Once served,the home page will be displayed which will include list of recipes returned by the API.

2.On clicking on particular recipe,then the detailed recipe will be shown.

3.Ingredients with a matching ingredientId listed in the specials response is integrated under ingredients name.

4.Clicking on Add recipes button in the home page, will be navigated to addrecipes page
